$(document).ready(function() {
    $(".liker__wrapper").on("click", function() {
        $(this).addClass("borered");
        $(".mdi.mdi-heart-outline").addClass("purple");
        $(".liker__wrapper_count").addClass("count-purple");
        let currentCount = $(".liker__wrapper_count").text();
        currentCount++;
        $(".liker__wrapper_count").text(currentCount);
    });
});