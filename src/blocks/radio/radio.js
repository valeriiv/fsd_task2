$(document).ready(() => {
    $(".radio__male_unselected").click(() => {
        $(".radio__male_unselected").hide();
        $(".radio__male_selected").show();
        $(".radio__female_unselected").show();
        $(".radio__female_selected").hide();
        $(".radio__female_text").css("color", "rgba(31, 32, 65, 0.5)");
        $(".radio__male_text").css("color", "rgba(31, 32, 65, 0.75)");
    });

    $(".radio__female_unselected").click(() => {
        $(".radio__female_unselected").hide();
        $(".radio__female_selected").show();
        $(".radio__male_selected").hide();
        $(".radio__male_unselected").show();
        $(".radio__female_text").css("color", "rgba(31, 32, 65, 0.75)");
        $(".radio__male_text").css("color", "rgba(31, 32, 65, 0.5)");
    });
});