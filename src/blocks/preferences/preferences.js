$(document).ready(() => {
    $(".preferences__smoke_unchecked").click(() => {
        $(".preferences__smoke_unchecked").hide();
        $(".preferences__smoke_checked").show();
    });

    $(".preferences__smoke_checked").click(() => {
        $(".preferences__smoke_checked").hide();
        $(".preferences__smoke_unchecked").show();
    });

    $(".preferences__animals_unchecked").click(() => {
        $(".preferences__animals_unchecked").hide();
        $(".preferences__animals_checked").show();
    });

    $(".preferences__animals_checked").click(() => {
        $(".preferences__animals_checked").hide();
        $(".preferences__animals_unchecked").show();
    });

    $(".preferences__guests_unchecked").click(() => {
        $(".preferences__guests_unchecked").hide();
        $(".preferences__guests_checked").show();
    });

    $(".preferences__guests_checked").click(() => {
        $(".preferences__guests_checked").hide();
        $(".preferences__guests_unchecked").show();
    });
});