$(document).ready(function() {
  let angle = 0;

  $(".pseudoselect_chevron").click(() => {
    $(".pseudooptions").slideToggle();

    angle -= 180;
    $(".pseudoselect_chevron").rotate(angle);
  });
});

function increaseFirst() {
  let currentCount = window.document.querySelector(
    ".pseudooptions__counter_count1"
  ).innerHTML;
  currentCount++;

  $(document).ready(function() {
    $(".pseudooptions__counter_count1").html(currentCount);

    $(".pseudooptions__counter_decrement1").css("opacity", "1");
  });

  checkClearHidden();
}

function decreaseFirst() {
  let currentCount = window.document.querySelector(
    ".pseudooptions__counter_count1"
  ).innerHTML;
  if (currentCount > 0) {
    currentCount--;

    $(document).ready(function() {
      $(".pseudooptions__counter_count1").html(currentCount);
    });
  }
  if (currentCount === 0) {
    $(document).ready(function() {
      $(".pseudooptions__counter_decrement1").css("opacity", "0.5");

      $(".pseudooptions__buttons_clear").css("visibility", "hidden");
    });
  }
}

function increaseSecond() {
  let currentCount = window.document.querySelector(
    ".pseudooptions__counter_count2"
  ).innerHTML;
  currentCount++;
  $(document).ready(function() {
    $(".pseudooptions__counter_count2").html(currentCount);

    $(".pseudooptions__counter_decrement2").css("opacity", "1");
  });

  checkClearHidden();
}

function decreaseSecond() {
  let currentCount = window.document.querySelector(
    ".pseudooptions__counter_count2"
  ).innerHTML;
  if (currentCount > 0) {
    currentCount--;

    $(document).ready(function() {
      $(".pseudooptions__counter_count2").html(currentCount);
    });
  }
  if (currentCount === 0) {
    $(document).ready(function() {
      $(".pseudooptions__counter_decrement2").css("opacity", "0.5");

      $(".pseudooptions__buttons_clear").css("visibility", "hidden");
    });
  }
}

function increaseThird() {
  let currentCount = window.document.querySelector(
    ".pseudooptions__counter_count3"
  ).innerHTML;
  currentCount++;
  $(document).ready(function() {
    $(".pseudooptions__counter_count3").html(currentCount);

    $(".pseudooptions__counter_decrement3").css("opacity", "1");
  });

  checkClearHidden();
}

function decreaseThird() {
  let currentCount = window.document.querySelector(
    ".pseudooptions__counter_count3"
  ).innerHTML;
  if (currentCount > 0) {
    currentCount--;

    $(document).ready(function() {
      $(".pseudooptions__counter_count3").html(currentCount);
    });
  }
  if (currentCount === 0) {
    $(document).ready(function() {
      $(".pseudooptions__counter_decrement3").css("opacity", "0.5");

      $(".pseudooptions__buttons_clear").css("visibility", "hidden");
    });
  }
}

function removeAll() {
  $(document).ready(function() {
    $(".pseudoselect_chooser").val("");
    $(".pseudooptions__counter_count1").html(0);
    $(".pseudooptions__counter_count2").html(0);
    $(".pseudooptions__counter_count3").html(0);
  });

  $(document).ready(function() {
      $(".pseudooptions__buttons_clear").css("visibility", "hidden");
  });
}

function apply() {
  $(document).ready(function() {
    let firstCounter = window.document.querySelector(
      ".pseudooptions__counter_count1"
    ).innerHTML;
    let secondCounter = window.document.querySelector(
      ".pseudooptions__counter_count2"
    ).innerHTML;
    let thirdCounter = window.document.querySelector(
      ".pseudooptions__counter_count3"
    ).innerHTML;
    let result =
      Number(firstCounter) + Number(secondCounter) + Number(thirdCounter);
    if (result === 1) {
      $(".pseudoselect_chooser").val(`${result} гость`);
    } else if (result >= 2 && result <= 4) {
      $(".pseudoselect_chooser").val(`${result} гостя`);
    } else {
      $(".pseudoselect_chooser").val(`${result} гостей`);
    }
  });
}

function checkClearHidden() {
  $(document).ready(function() {
    let visibilityStatus = $(".pseudooptions__buttons_clear").css("visibility");
    if (visibilityStatus === "hidden") {
      $(".pseudooptions__buttons_clear").css("visibility", "visible");
    }
  });
}