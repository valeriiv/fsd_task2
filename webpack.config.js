const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const ETP = require("extract-text-webpack-plugin");

const config = {
  entry: "./index.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.pug$/,
        loader: "pug-loader",
        options: {
          pretty: true
        }
      },
      {
        test: /\.(css|sass|scss)$/,
        loader: ETP.extract({
          fallback: "style-loader",
          use: "css-loader!sass-loader"
        })
      }
    ]
  },
  devServer: {
    compress: true,
    port: 8099,
    noInfo: true,
    open: true
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: "Cards",
      minify: {
        collapseWhitespace: true
      },
      hash: true,
      template: "./src/pages/index.pug"
    }),
    new ETP("styles.css")
  ]
};

module.exports = config;
